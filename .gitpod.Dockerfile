FROM gitpod/workspace-full:latest

RUN sudo install-packages tree 
RUN git config --global pull.rebase false
RUN git config --global fetch.prune true
RUN git config --global diff.colorMoved zebra
RUN sudo install-packages maven